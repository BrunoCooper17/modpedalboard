#include "ParentSocket.h"
#include "Constants.h"

#include <unistd.h>
#include <cstring>
#include <sys/socket.h>
#include <iostream>


ModPedalboard::ParentSocket::ParentSocket() {
	fd = -1;
}

ModPedalboard::ParentSocket::~ParentSocket() {
	CloseSocket();
}

bool ModPedalboard::ParentSocket::IsOpen() {
	return (fd != -1 ? true : false);
}

void ModPedalboard::ParentSocket::CloseSocket() {
	if(fd != -1) {
		close(fd);
		fd = -1;
	}
}

bool ModPedalboard::ParentSocket::SendData(std::string Data) {
	int SIZE_DATA = 0;
	if((SIZE_DATA = write(fd, Data.c_str(), Data.length())) < 0) {
		perror("SENDING DATA");
		return false;
	}
	
	//std::cout << "DATA SENT -> " << SIZE_DATA << std::endl;
	
	return true;
}

std::string ModPedalboard::ParentSocket::ReadData() {
	char Buffer[BUFFER_SIZE] = {0};
	bzero(Buffer, BUFFER_SIZE);
	if (read(fd, Buffer, BUFFER_SIZE) < 0) {
		perror("ERROR read from socket");
		return std::string();
	}
	return std::string(Buffer);
}
