#pragma once

#include "ParentSocket.h"

// Esta clase se empleara para establecer comunicacion con la aplicacion
// servidor MOD.

namespace ModPedalboard
{

class Socket : public ParentSocket
{
private:
	unsigned int Port = 0;	// Port to connect.
	std::string Hostname;	
	bool bError;
	
public:
	Socket(std::string Host, unsigned int Port);

	virtual void OpenSocket();
	virtual bool IsOpen();
};

}

