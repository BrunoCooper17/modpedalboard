#include "Socket.h"

#include <cstdlib>
#include <cstring>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include <iostream>

ModPedalboard::Socket::Socket(std::string Host, unsigned int Port) : ParentSocket() {
	this->Hostname = Host;
	this->Port = Port;
	
	bError = false;
}

void ModPedalboard::Socket::OpenSocket() {
	std::cout << "CONNECTING TO MOD-HOST AT " << Hostname << " WITH PORT " << Port << std::endl;
	
	struct sockaddr_in serv_addr;
    struct hostent *server;

	bError = false;

	fd = socket(AF_INET, SOCK_STREAM, 0);
	if(fd < 0) {
		perror("OPENING SOCKET");
		bError = true;
		return;
	}
	
	server = gethostbyname(Hostname.c_str());
	if(server == nullptr) {
		perror("HOST NOT FOUND");
		bError = true;
		return;
	}

	bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
    serv_addr.sin_port = htons(Port);

    if (connect(fd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) {
		perror("ERROR CONNECTING");
		bError = true;
		return;
	}
}

bool ModPedalboard::Socket::IsOpen() {
	return (ModPedalboard::ParentSocket::IsOpen() || !bError);
}
