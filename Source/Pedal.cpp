#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <list>

#include "Pedal.h"
#include "Constants.h"

#include <iostream>

void ModPedalboard::Pedal::setURI(std::string URI) {
	this->Uri = URI;
}


void ModPedalboard::Pedal::LoadInfo() {
	std::string tmpCommand("lv2info ");
	tmpCommand.append(Uri);

	FILE* fp = popen(tmpCommand.c_str(), "r");
	if(fp == nullptr) {
		perror("ERROR Executing lv2ls command");
		return;
	}

	bool bName = false;
	bool bClass = false;
	char Buffer[BUFFER_SIZE];
	char BufferCpy[BUFFER_SIZE];

	while(fgets(Buffer, sizeof(Buffer), fp)) {
		strcpy(BufferCpy, Buffer);
		char* next_Tok;
		char* tmpTok = strtok_r(Buffer, " \t", &next_Tok);
		if(!bName && strcmp("Name:", tmpTok) == 0) {
			tmpTok = strtok_r(nullptr, " \t", &next_Tok);
			Name = std::string(GetStringFrom(BufferCpy, tmpTok));

			bName = true;
		}
		else if(!bClass && strcmp("Class:", tmpTok) == 0) {
			tmpTok = strtok_r(nullptr, " \t", &next_Tok);
			Class = std::string(GetStringFrom(BufferCpy, tmpTok));

			bClass = true;
		}
		else if(strcmp("Port", tmpTok) == 0) {
			PortDetails tmpPort;
			bool bFinishPort = false;
			char* LastString;
			do {
				while((LastString = fgets(Buffer, sizeof(Buffer), fp))) {
					strcpy(BufferCpy, Buffer);
					char* tmpnext_Tok;
					char* tmptmpTok = strtok_r(Buffer, " \t", &tmpnext_Tok);
					
					if(strcmp("Type:", tmptmpTok) == 0) {
						tmptmpTok = strtok_r(nullptr, " \t", &tmpnext_Tok);
						std::string type01 = GetStringFrom(BufferCpy, tmptmpTok);
						
						fgets(Buffer, sizeof(Buffer), fp);
						strcpy(BufferCpy, Buffer);
						tmptmpTok = strtok_r(Buffer, " \t", &tmpnext_Tok);
						std::string type02 = GetStringFrom(BufferCpy, tmptmpTok);
						
						tmpPort.SetPortType(type01, type02);
					}
					else if(strcmp("Symbol:", tmptmpTok) == 0) {
						tmptmpTok = strtok_r(nullptr, " \t", &tmpnext_Tok);
						tmpPort.SetPortSymbol(GetStringFrom(BufferCpy, tmptmpTok));
					}
					else if(strcmp("Name:", tmptmpTok) == 0) {
						tmptmpTok = strtok_r(nullptr, " \t", &tmpnext_Tok);
						tmpPort.SetPortName(GetStringFrom(BufferCpy, tmptmpTok));
					}
					else if(strcmp("Minimum:", tmptmpTok) == 0) {
						tmptmpTok = strtok_r(nullptr, " \t", &tmpnext_Tok);
						tmpPort.SetMinimumValue(std::atof(GetStringFrom(BufferCpy, tmptmpTok).c_str()));
					}
					else if(strcmp("Maximum:", tmptmpTok) == 0) {
						tmptmpTok = strtok_r(nullptr, " \t", &tmpnext_Tok);
						tmpPort.SetMaximumValue(std::atof(GetStringFrom(BufferCpy, tmptmpTok).c_str()));
					}
					else if(strcmp("Default:", tmptmpTok) == 0) {
						tmptmpTok = strtok_r(nullptr, " \t", &tmpnext_Tok);
						std::string tmpStr = GetStringFrom(BufferCpy, tmptmpTok);
						tmpPort.SetDefaultValue(std::atof(tmpStr.c_str()));
						tmpPort.SetCurrentValue(std::atof(tmpStr.c_str()));
					}
					else if(strcmp("Scale", tmptmpTok) == 0) {
						//std::cout << "SCALE" << std::endl;
						tmpPort.SetScalarValue(true);
						while(fgets(Buffer, sizeof(Buffer), fp)) {
							char* tmptmpnext_Tok;
							char* tmptmptmpTok = strtok_r(Buffer, "\t", &tmptmpnext_Tok);
							if(strlen(tmptmptmpTok) == 1) {
								break;
							}
							else {
								int IndexScalar;
								char ScalarLabel[100];
								bzero(ScalarLabel, 100);
								sscanf(Buffer, "%d = %[^\n]", &IndexScalar, ScalarLabel);
								//std::cout << IndexScalar << "<->" << ScalarLabel << std::endl;
								tmpPort.AddScalarValue(IndexScalar, ScalarLabel);
							}
						}
						continue;
					}
					else if(strcmp("Properties:", tmptmpTok) == 0) {
						tmptmpTok = strtok_r(nullptr, " \t", &tmpnext_Tok);
						tmpPort.ProcessProperty(GetStringFrom(BufferCpy, tmptmpTok));
						
						while((LastString = fgets(Buffer, sizeof(Buffer), fp))) {
							strcpy(BufferCpy, Buffer);
							char* tmptmpnext_Tok;
							char* tmptmptmpTok = strtok_r(Buffer, " \t", &tmptmpnext_Tok);
							
							if(strlen(tmptmptmpTok) == 1) {
								break;
							}
							
							tmpPort.ProcessProperty(GetStringFrom(BufferCpy, tmptmptmpTok));
						}
						continue;
					}
					else if(strlen(tmptmpTok) == 1) {
						bFinishPort = true;
						break;
					}
				}
			} while(!bFinishPort && LastString != nullptr);
			switch(tmpPort.GetPortType()) {
				case ModPedalboard::EPortType::EPT_Atom:
					PortsAtom.push_back(tmpPort);
					break;
				case ModPedalboard::EPortType::EPT_Audio:
					PortsAudio.push_back(tmpPort);
					break;
				case ModPedalboard::EPortType::EPT_Control:
					PortsControl.push_back(tmpPort);
					break;
			}
		}
	}

	fclose(fp);
}


std::string ModPedalboard::Pedal::GetStringFrom(std::string str, std::string from) {
	//printf("---%s---%s---", str.c_str(), from.c_str());
	char Buffer[BUFFER_SIZE];
	std::string tmpStr = str.substr(str.find(from));
	sscanf(tmpStr.c_str(), "%[^\n]", Buffer);
	return std::string(Buffer);
}


std::string ModPedalboard::Pedal::GetURI() {
	return Uri;
}


std::string ModPedalboard::Pedal::GetName() {
	return Name;
}


std::string ModPedalboard::Pedal::GetClass() {
	return Class;
}


void ModPedalboard::Pedal::StreamPortsInformation(std::stringstream& strstream) {
	StreamPortInfo(PortsAtom, strstream);
	strstream << "||";
	//printf("|");
	StreamPortInfo(PortsAudio, strstream);
	strstream << "||";
	//printf("|");
	StreamPortInfo(PortsControl, strstream);
}


void ModPedalboard::Pedal::StreamPortInfo(std::vector<PortDetails> PortList, std::stringstream& strstream) {
	int PortCont = PortList.size();
	for(int Index=0; Index < PortCont; Index++) {
		PortDetails tmpPort = PortList[Index];
		strstream << "P$$" << Index << "&&S$$"<< tmpPort.GetPortSymbol() <<"&&N$$" << tmpPort.GetPortName()
			<< "&&PT$$" << tmpPort.GetPortType() << "&&PF$$" << tmpPort.GetPortFlow() << "&&m$$" << tmpPort.GetMinimum()
			<< "&&M$$" << tmpPort.GetMaximum() << "&&D$$" << tmpPort.GetDefault() << "&&SC$$" << tmpPort.GetScalarLabelsCount()
			<< "&&PS$$" << tmpPort.IsScalarValue() << "&&PI$$" << tmpPort.IsIntegerValue() << "&&T$$" << tmpPort.IsToggleValue();

/**
		printf("P$%d::S$%s::N$%s::PT$%d::PF$%d::m$%0.2f::M$%0.2f::D$%0.2f::S$%d::PS$%d::PI$%d::T$%d",
			Index, tmpPort.GetPortSymbol().c_str(), tmpPort.GetPortName().c_str(), tmpPort.GetPortType(),
			tmpPort.GetPortFlow(), tmpPort.GetMinimum(), tmpPort.GetMaximum(), tmpPort.GetDefault(),
			tmpPort.GetScalarLabelsCount(), tmpPort.IsScalarValue(), tmpPort.IsIntegerValue(),
			tmpPort.IsToggleValue()
		);
**/

		int LabelsTotal = tmpPort.GetScalarLabelsCount();
		if(LabelsTotal) {
			//printf("SCALAR LABEL(S) -> ");
			//printf("::SL$");
			strstream << "&&SL$$";
			
			for(int LabelIndex=0; LabelIndex<LabelsTotal; LabelIndex++) {
				strstream << LabelIndex << "%%" << tmpPort.GetTextLabelWithIndex(LabelIndex);
				//printf("%d %s", LabelIndex, tmpPort.GetTextLabelWithIndex(LabelIndex).c_str());
				if(LabelIndex+1 < LabelsTotal) {
					strstream << "==";
					//printf("=");
				}
			}
		}
		if( Index+1 < PortCont) {
			strstream << "||";
			//printf("|");
		}
	}
}
