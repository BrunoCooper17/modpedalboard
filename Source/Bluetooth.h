/*
  BlueZ example code to build an rfcomm server.
  This code just creates a socket and accepts
  connections from a remote bluetooth device.
  Programmed by Bastian Ballmann
  http://www.geektown.de
  Compile with gcc -lbluetooth <executable> <source>
*/

#pragma once

#include "ParentSocket.h"
#include <bluetooth/sdp.h>
#include <bluetooth/sco.h>
#include <bluetooth/sdp_lib.h>

// Clase responsable de la comunicación con dispositivos bluetooth Android
// A travez de esta clase se enviaran y recibiran los parametros de los pedales.

namespace ModPedalboard
{

class Bluetooth : public ParentSocket
{
private:
	int Client;
	int fdbk;
	int PortService;
	std::string uuid_service;
	sdp_session_t* SessionService;
	
public:
	Bluetooth(std::string uuid_service, int PortService);
	virtual ~Bluetooth();

	virtual void OpenSocket();
	bool WaitConnection(int timeout_sec);
	void CloseClient();
	
private:
	int Str2uuid( const char *uuid_str, uuid_t *uuid );
	sdp_session_t* register_service(const char* uuid, uint8_t rfcomm_channel);
};

}

