#include "Bluetooth.h"

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/l2cap.h>

#include <iostream>

/*
 * Reference: 
 * http://stackoverflow.com/questions/3255406/trouble-connecting-android-bluetooth-client-socket-to-ubuntu-server-socket
 * */

ModPedalboard::Bluetooth::Bluetooth(std::string uuid_service, int PortService) : ParentSocket() {
	Client = -1;
	this->PortService= PortService;
	this->uuid_service = uuid_service;

	this->SessionService = register_service(uuid_service.c_str(), PortService);
}

ModPedalboard::Bluetooth::~Bluetooth() {
	CloseClient();
	sdp_close( SessionService );
}

void ModPedalboard::Bluetooth::OpenSocket() {
	struct sockaddr_rc addr;
	//const int CHANNEL = 4;
	
	
	bdaddr_t btAddress;
	int dev_id = hci_get_route(NULL);
	hci_devba(dev_id, &btAddress);
	
	//char str_btAddress[20];
	//ba2str(&btAddress, str_btAddress);
	//printf("BT ADDR -> .%s.\n", str_btAddress);
	
	
	if((fd = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM)) < 0) {
		printf("Error: socket");
		exit(1);
	}
	
	addr.rc_family = AF_BLUETOOTH;
	
	//bdaddr_t tmp = { };
	//bacpy(&addr.rc_bdaddr, BDADDR_ANY);
	bacpy(&addr.rc_bdaddr, &btAddress);
	addr.rc_channel = htobs(PortService);
	
	int alen = sizeof(addr);

	if(bind(fd, (struct sockaddr *)&addr, alen) < 0) {
		printf("Error: bind");
		bError = false;
		return;
	}

	listen(fd, 1);
}

bool ModPedalboard::Bluetooth::WaitConnection(int timeout_sec) {
	fd_set fds;
	int res;
	struct timeval tv;
 
	FD_ZERO(&fds);
	FD_SET(fd, &fds);
	
	tv.tv_sec = timeout_sec;
	tv.tv_usec = 0;
 
	system("sudo hciconfig hci0 up");
	system("sudo hciconfig hci0 piscan");
	system("sudo btmgmt -i hci0 connectable on");
	system("sudo btmgmt -i hci0 fast-conn on");
	res = select (fd + 1, &fds, NULL, NULL, &tv);
	system("sudo hciconfig hci0 noscan");
	if (res <= 0 && errno != EINTR) {
		std::cout << "TIMEOUT 1" << std::endl;
		perror ("select");
		system("sudo btmgmt -i hci0 connectable off");
		system("sudo btmgmt -i hci0 fast-conn off");
		return false;
	}
 
	if (FD_ISSET(fd, &fds)) {
		struct sockaddr_in client_addr;
		memset(&client_addr, 0, sizeof(client_addr));
		socklen_t client_len = sizeof(client_addr);
		Client = accept(fd, (struct sockaddr *)&client_addr, &client_len);
		
		if(Client < 0) {
			system("sudo btmgmt -i hci0 connectable off");
			system("sudo btmgmt -i hci0 fast-conn off");
			return false;
		}
		
		fdbk = fd;
		fd = Client;
	}
	else {
		std::cout << "TIMEOUT 2" << std::endl;
		system("sudo btmgmt -i hci0 connectable off");
		system("sudo btmgmt -i hci0 fast-conn off");
		return false;
	}
	
	std::cout << "CLIENT CONNECTED" << std::endl;
	
	system("sudo btmgmt -i hci0 connectable off");
	system("sudo btmgmt -i hci0 fast-conn off");
	return true;
}

void ModPedalboard::Bluetooth::CloseClient() {
	if(Client >= 0) {
		close(Client);
		Client = -1;
		fd = fdbk;
	}
}

int ModPedalboard::Bluetooth::Str2uuid( const char *uuid_str, uuid_t *uuid ) {
	/* This is from the pybluez stack */
	uint32_t uuid_int[4];
	char *endptr;

	if( strlen( uuid_str ) == 36 ) {
		char buf[9] = { 0 };

		if( uuid_str[8] != '-' && uuid_str[13] != '-' &&
		uuid_str[18] != '-' && uuid_str[23] != '-' ) {
		return 0;
	}
	// first 8-bytes
	strncpy(buf, uuid_str, 8);
	uuid_int[0] = htonl( strtoul( buf, &endptr, 16 ) );
	if( endptr != buf + 8 ) return 0;
		// second 8-bytes
		strncpy(buf, uuid_str+9, 4);
		strncpy(buf+4, uuid_str+14, 4);
		uuid_int[1] = htonl( strtoul( buf, &endptr, 16 ) );
		if( endptr != buf + 8 ) return 0;

		// third 8-bytes
		strncpy(buf, uuid_str+19, 4);
		strncpy(buf+4, uuid_str+24, 4);
		uuid_int[2] = htonl( strtoul( buf, &endptr, 16 ) );
		if( endptr != buf + 8 ) return 0;

		// fourth 8-bytes
		strncpy(buf, uuid_str+28, 8);
		uuid_int[3] = htonl( strtoul( buf, &endptr, 16 ) );
		if( endptr != buf + 8 ) return 0;

		if( uuid != NULL ) sdp_uuid128_create( uuid, uuid_int );
	} else if ( strlen( uuid_str ) == 8 ) {
		// 32-bit reserved UUID
		uint32_t i = strtoul( uuid_str, &endptr, 16 );
		if( endptr != uuid_str + 8 ) return 0;
		if( uuid != NULL ) sdp_uuid32_create( uuid, i );
	} else if( strlen( uuid_str ) == 4 ) {
		// 16-bit reserved UUID
		int i = strtol( uuid_str, &endptr, 16 );
		if( endptr != uuid_str + 4 ) return 0;
		if( uuid != NULL ) sdp_uuid16_create( uuid, i );
	} else {
		return 0;
	}
	return 1;
}

sdp_session_t* ModPedalboard::Bluetooth::register_service(const char* uuid, uint8_t rfcomm_channel)
{
	/*
	 * C++ disallows taking addresses of temporaries. See
	 * http://stackoverflow.com/questions/9751710/c-c-warning-address-of-temporary-with-bdaddr-any-bluetooth-library
	 */
 
	bdaddr_t my_bdaddr_any = {0, 0, 0, 0, 0, 0};
	bdaddr_t my_bdaddr_local = {0, 0, 0, 0xff, 0xff, 0xff};
	
	// Adapted from http://www.btessentials.com/examples/bluez/sdp-register.c
	//uint32_t svc_uuid_int[] = {   0x01110000, 0x00100000, 0x80000080, 0xFB349B5F };
	const char *service_name = "ModPedalboard_Bluetooth_Service";
	const char *svc_dsc = "My ModPedalboard Bluetooth Service";
	const char *service_prov = "ModPedalboard";

	uuid_t root_uuid, l2cap_uuid, rfcomm_uuid, svc_uuid, svc_class_uuid;
	sdp_list_t *l2cap_list = 0,
			   *rfcomm_list = 0,
			   *root_list = 0,
			   *proto_list = 0,
			   *access_proto_list = 0,
			   *svc_class_list = 0,
			   *profile_list = 0;
	sdp_data_t *channel = 0;
	sdp_profile_desc_t profile;
	sdp_record_t record = { 0 };
	sdp_session_t *session = 0;

	// set the general service ID
	//sdp_uuid128_create( &svc_uuid, &svc_uuid_int );
	Str2uuid(uuid, &svc_uuid);
	sdp_set_service_id( &record, svc_uuid );

	char str[256] = "";
	sdp_uuid2strn(&svc_uuid, str, 256);
	printf("Registering UUID %s\n", str);

	// set the service class
	sdp_uuid16_create(&svc_class_uuid, SERIAL_PORT_SVCLASS_ID);
	svc_class_list = sdp_list_append(0, &svc_class_uuid);
		svc_class_list = sdp_list_append(svc_class_list, &svc_uuid);
	sdp_set_service_classes(&record, svc_class_list);

	// set the Bluetooth profile information
	sdp_uuid16_create(&profile.uuid, SERIAL_PORT_PROFILE_ID);
	profile.version = 0x0100;
	profile_list = sdp_list_append(0, &profile);
	sdp_set_profile_descs(&record, profile_list);

	// make the service record publicly browsable
	sdp_uuid16_create(&root_uuid, PUBLIC_BROWSE_GROUP);
	root_list = sdp_list_append(0, &root_uuid);
	sdp_set_browse_groups( &record, root_list );

		// set l2cap information
		sdp_uuid16_create(&l2cap_uuid, L2CAP_UUID);
		l2cap_list = sdp_list_append( 0, &l2cap_uuid );
		proto_list = sdp_list_append( 0, l2cap_list );

	// register the RFCOMM channel for RFCOMM sockets
	sdp_uuid16_create(&rfcomm_uuid, RFCOMM_UUID);
	channel = sdp_data_alloc(SDP_UINT8, &rfcomm_channel);
	rfcomm_list = sdp_list_append( 0, &rfcomm_uuid );
	sdp_list_append( rfcomm_list, channel );
	sdp_list_append( proto_list, rfcomm_list );

	access_proto_list = sdp_list_append( 0, proto_list );
	sdp_set_access_protos( &record, access_proto_list );

	// set the name, provider, and description
	sdp_set_info_attr(&record, service_name, service_prov, svc_dsc);

	// connect to the local SDP server, register the service record,
	// and disconnect
	session = sdp_connect(&my_bdaddr_any, &my_bdaddr_local, SDP_RETRY_IF_BUSY);
	sdp_record_register(session, &record, 0);

	// cleanup
	sdp_data_free( channel );
	sdp_list_free( l2cap_list, 0 );
	sdp_list_free( rfcomm_list, 0 );
	sdp_list_free( root_list, 0 );
	sdp_list_free( access_proto_list, 0 );
	sdp_list_free( svc_class_list, 0 );
	sdp_list_free( profile_list, 0 );

	return session;
}
