#pragma once

#include <string>
#include <vector>
#include <sstream>
#include "PortDetails.h"
#include "Mod-Host/effects.h"

namespace ModPedalboard
{

class Pedal {
// METHODS
public:
	void setURI(std::string URI);

	void LoadInfo();

	std::string GetURI();
	std::string GetName();
	std::string GetClass();
	void StreamPortsInformation(std::stringstream& strstream);

private:
	std::string GetStringFrom(std::string str, std::string from);
	void StreamPortInfo(std::vector<PortDetails> PortList, std::stringstream& strstream);

// VARIABLES
//private:
	std::string Uri;
	std::string Name;
	std::string Class;

	class std::vector<PortDetails> PortsControl;
	class std::vector<PortDetails> PortsAudio;
	class std::vector<PortDetails> PortsAtom;
};

}
