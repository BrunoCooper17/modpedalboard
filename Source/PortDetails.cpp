#include "PortDetails.h"

ModPedalboard::PortDetails::PortDetails() {
	this->bScalarValue = false;
	this->bIntegerValue = false;
	this->bToggleValue = false;
}

void ModPedalboard::PortDetails::SetPortSymbol(std::string Symbol) {
	this->PortSymbol = Symbol;
}

void ModPedalboard::PortDetails::SetPortName(std::string Name) {
	this->PortName = Name;
}

void ModPedalboard::PortDetails::SetPortType(std::string Type01, std::string Type02) {
	if(Type01.compare(STRING_ATOM_PORT) == 0 || Type02.compare(STRING_ATOM_PORT) == 0) {
		this->PortType = ModPedalboard::EPortType::EPT_Atom;
	}
	// IS AN AUDIO PORT?
	if(Type01.compare(STRING_AUDIO_PORT) == 0 || Type02.compare(STRING_AUDIO_PORT) == 0) {
		this->PortType = ModPedalboard::EPortType::EPT_Audio;
	}
	// IS AN CONTROL PORT?
	else if(Type01.compare(STRING_CONTROL_PORT) == 0 || Type02.compare(STRING_CONTROL_PORT) == 0) {
		this->PortType = ModPedalboard::EPortType::EPT_Control;
	}
	
	// INPUT OR OUTPUT???
	if(Type01.compare(STRING_PORT_INPUT) == 0 || Type02.compare(STRING_PORT_INPUT) == 0) {
		this->PortFlow = ModPedalboard::EPortFlow::EPF_Input;
	}
	else if(Type01.compare(STRING_PORT_OUTPUT) == 0 || Type02.compare(STRING_PORT_OUTPUT) == 0) {
		this->PortFlow = ModPedalboard::EPortFlow::EPF_Output;
	}
}

void ModPedalboard::PortDetails::SetScalarValue(bool bScalar) {
	this->bScalarValue = bScalar;
}

void ModPedalboard::PortDetails::AddScalarValue(int Index, std::string TextValue) {
	if(Index < 0) return;
	if((Index+1) > (int)TextScalarValues.size()) {
		TextScalarValues.resize(Index+1);
	}
	TextScalarValues[Index] = TextValue;
}

void ModPedalboard::PortDetails::ProcessProperty(std::string Property) {
	if(Property.compare(STRING_PROPERTY_INTEGER) == 0) {
		bIntegerValue = true;
	}
	else if(Property.compare(STRING_PROPERTY_TOGGLE) == 0) {
		bToggleValue = true;
	}
}

void ModPedalboard::PortDetails::SetMinimumValue(float Min) {
	this->Minimum = Min;
}

void ModPedalboard::PortDetails::SetMaximumValue(float Max) {
	this->Maximum = Max;
}

void ModPedalboard::PortDetails::SetDefaultValue(float Default) {
	this->Default = Default;
}

void ModPedalboard::PortDetails::SetCurrentValue(float Current) {
	this->Current = Current;
}
