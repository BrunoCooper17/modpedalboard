#pragma once

#include <string>
#include <vector>

namespace ModPedalboard
{

enum EPortType {
	EPT_Atom,
	EPT_Audio,
	EPT_Control
};

enum EPortFlow {
	EPF_Input,
	EPF_Output
};

class PortDetails {
private:
	std::string PortSymbol;
	std::string PortName;
	
	EPortType PortType;
	EPortFlow PortFlow;

	bool bToggleValue;
	bool bIntegerValue;
	bool bScalarValue;
	float Minimum, Maximum, Default, Current;
	
	std::vector<std::string> TextScalarValues;

	const char* STRING_ATOM_PORT	 = "http://lv2plug.in/ns/ext/atom#AtomPort";
	const char* STRING_CONTROL_PORT	 = "http://lv2plug.in/ns/lv2core#ControlPort";
	const char* STRING_AUDIO_PORT	 = "http://lv2plug.in/ns/lv2core#AudioPort";
	const char* STRING_PORT_INPUT	 = "http://lv2plug.in/ns/lv2core#InputPort";
	const char* STRING_PORT_OUTPUT	 = "http://lv2plug.in/ns/lv2core#OutputPort";
	
	const char* STRING_PROPERTY_INTEGER	 = "http://lv2plug.in/ns/lv2core#integer";
	const char* STRING_PROPERTY_TOGGLE	 = "http://lv2plug.in/ns/lv2core#toggled";

public:
	PortDetails();

	void SetPortSymbol(std::string Symbol);
	void SetPortName(std::string Name);
	void SetPortType(std::string Type01, std::string Type02);
	
	void SetScalarValue(bool bScalar);
	void AddScalarValue(int Index, std::string TextValue);
	void ProcessProperty(std::string Property);
	
	void SetMinimumValue(float Min);
	void SetMaximumValue(float Max);
	void SetDefaultValue(float Default);
	void SetCurrentValue(float Value);
	
	std::string GetPortSymbol() { return PortSymbol; }
	std::string GetPortName() { return PortName; }
	
	EPortType GetPortType() { return PortType; }
	EPortFlow GetPortFlow() { return PortFlow; }

	bool IsToggleValue() { return bToggleValue; }
	bool IsIntegerValue() { return bIntegerValue; }
	bool IsScalarValue() { return bScalarValue; }
	float GetMinimum() { return Minimum; }
	float GetMaximum() { return Maximum; }
	float GetDefault() { return Default; }
	float GeCurrent() { return Current; }
	
	int GetScalarLabelsCount() { return TextScalarValues.size(); }
	std::string GetTextLabelWithIndex(int Index) { return TextScalarValues[Index]; }
};

}
