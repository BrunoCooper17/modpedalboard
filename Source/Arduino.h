/* www.chrisheydrick.com
     
   June 23 2012

   CanonicalArduinoRead write a byte to an Arduino, and then
   receives a serially transmitted string in response.

   The call/response Arduino sketch is here:
   https://gist.github.com/2980344

   Arduino sketch details at www.chrisheydrick.com
*/

#pragma once

#include "ParentSocket.h"

// Clase que establece la comunicacion con la placa arduino.
// Recibe el input y envia datos para la muestra de pedales activos
// Asi como mensajes que tengan que desplegarse.

namespace ModPedalboard
{

class Arduino : public ParentSocket
{
private:
	std::string Device;

	void SetAttributes();
	
public:
	Arduino(std::string Device);
	
	virtual void OpenSocket();
};

}

