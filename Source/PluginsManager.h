#pragma once

#include <map>
#include <string>
#include <vector>
#include <sstream>

#include "Constants.h"
#include "Pedal.h"

namespace ModPedalboard
{
	
class PluginsManager {
// METHODS
public:
	PluginsManager();
	
	// MULTITHREAD LOADING
	void PreparePlugins_Data();
	void LoadPlugins_Thread();
	void UnifyPlugins_Data();

	// SINGLE THREAD LOADING
	void LoadPlugins();
	void LoadPresets();
	
	void PrintPlugins(std::stringstream& strstream);

// VARIABLES
private:
	std::vector<std::string> tmpURI_Chunk[THREADS_MAX];
	std::vector<Pedal> tmpPlugins_Chunk[THREADS_MAX];

	std::map<std::string, std::vector<Pedal>> PluginList;
//	class std::list<class Preset> PresetList;

	void ProcessChunk(int Index);
};

}
