#pragma once

#include <string>

namespace ModPedalboard
{

class ParentSocket
{
protected:
	int fd;
	bool bError;
	
public:
	ParentSocket();
	virtual ~ParentSocket();

	virtual void OpenSocket() = 0;
	void CloseSocket();

	virtual bool IsOpen();

	bool SendData(std::string Data);
	std::string ReadData();
};

}

