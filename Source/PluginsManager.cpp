#include <cstdio>
#include <cstring>
#include <iostream>
#include <list>
#include <thread>
#include <unistd.h>

#include "PluginsManager.h"

ModPedalboard::PluginsManager::PluginsManager() {
}

void ModPedalboard::PluginsManager::PreparePlugins_Data() {
	FILE* fp = popen("lv2ls", "r");
	if(fp == nullptr) {
		perror("ERROR Failed to execute lv2ls");
		return;
	}

	char Buffer[200];
	int PluginsPreLoaded=0;

	while(fgets(Buffer, sizeof(Buffer), fp)) {
		tmpURI_Chunk[PluginsPreLoaded % THREADS_MAX].push_back(std::string(strtok(Buffer, "\n")));
		PluginsPreLoaded++;
	}
	
	for(unsigned int Index = 0; Index < THREADS_MAX; Index++) {
		tmpPlugins_Chunk[Index].resize(tmpURI_Chunk[Index].size());
	}

	fclose(fp);
}

void ModPedalboard::PluginsManager::LoadPlugins_Thread() {
	std::vector<std::thread> threads;
	for(unsigned int Index = 0; Index < THREADS_MAX; Index++) {
		threads.push_back(std::thread(&ModPedalboard::PluginsManager::ProcessChunk, this, Index));
	}

	for(unsigned int i = 0; i < threads.size() ; i++) {
		threads.at(i).join();
	}
}

void ModPedalboard::PluginsManager::UnifyPlugins_Data() {
	int pluginsloaded = 0;
	for(unsigned int Index = 0; Index < THREADS_MAX; Index++) {
		int ChunkSize = tmpPlugins_Chunk[Index].size();
		for(int LocalIndex = 0; LocalIndex<ChunkSize; LocalIndex++) {
			Pedal tmpPlugin = tmpPlugins_Chunk[Index][LocalIndex];
			PluginList[tmpPlugin.GetClass()].push_back(tmpPlugin);
			pluginsloaded++;
		}
	}
	printf("Plugins Loaded %d\n", pluginsloaded);
}

void ModPedalboard::PluginsManager::LoadPlugins() {
	FILE* fp = popen("lv2ls", "r");
	if(fp == nullptr) {
		perror("ERROR Failed to execute lv2ls");
		return;
	}

	char Buffer[200];
	char* tmpUri = nullptr;
	
	int pluginsloaded=0;

	while(fgets(Buffer, sizeof(Buffer), fp)) {
		tmpUri = strtok(Buffer, "\n");
		Pedal tmpPlugin;
		tmpPlugin.setURI(tmpUri);
		tmpPlugin.LoadInfo();

		PluginList[tmpPlugin.GetClass()].push_back(tmpPlugin);
		pluginsloaded++;

/**
		printf("URI -> %s\nEfecto -> Nombre: %s\t Clase: %s\n",
			tmpPlugin.GetURI().c_str(), tmpPlugin.GetName().c_str(), tmpPlugin.GetClass().c_str());
		tmpPlugin.PrintPortsInformation();
		printf("\n");
**/
	}
	
	printf("Plugins Loaded %d\n", pluginsloaded);

	fclose(fp);
}


void ModPedalboard::PluginsManager::LoadPresets() {
}

void ModPedalboard::PluginsManager::PrintPlugins(std::stringstream& strstream) {
	std::map<std::string, std::vector<Pedal>>::iterator it = PluginList.begin();
	
	int IndexPedal = 0;
	for( ;it != PluginList.end(); ++it) {
		int PluginsClassCount = PluginList[it->first].size();
		//printf("Plugins Class: \"%s\"\n",it->first.c_str());
		printf("%d C$$%s?? PedalesCount %d\n", ++IndexPedal, it->first.c_str(), PluginsClassCount);
		strstream << "C$$" << it->first << "]]";
		for(int Count=0; Count<PluginsClassCount; Count++) {
			/*printf("Nombre: \"%s\"\tURI -> %s\n", PluginList[it->first][Count].GetName().c_str(),
				PluginList[it->first][Count].GetURI().c_str());*/
			//printf("Nmbr$%s::URI$%s@", PluginList[it->first][Count].GetName().c_str(),
			//	PluginList[it->first][Count].GetURI().c_str());
			strstream << "Nmbr$$" << PluginList[it->first][Count].GetName() 
				<< "&&URI$$" << PluginList[it->first][Count].GetURI() << "@@";

			PluginList[it->first][Count].StreamPortsInformation(strstream);
			if(Count+1 < PluginsClassCount) {
				strstream << "**";
			}
		}
		// $ ?? $ :: $ @ $ :: $ % =  |  $ :: $ % =   &&
		if(std::next(it, 1) != PluginList.end()) {
			strstream << "[[";
			//printf("\n");
		}
	}
}

void ModPedalboard::PluginsManager::ProcessChunk(int Index) {
	int SizeChunk = tmpURI_Chunk[Index].size();
	
	for(int LocalIndex=0; LocalIndex<SizeChunk; LocalIndex++) {
		Pedal tmpPlugin;
		tmpPlugin.setURI(tmpURI_Chunk[Index][LocalIndex]);
		tmpPlugin.LoadInfo();
		tmpPlugins_Chunk[Index][LocalIndex] = tmpPlugin;
	}
	//std::cout << "Processed " << SizeChunk << " at Index " << Index << std::endl;
}