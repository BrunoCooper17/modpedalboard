#include "Arduino.h"
#include <cstdlib>
#include <cstdio>
#include <cerrno>
#include <cstdint>

#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/ioctl.h>

ModPedalboard::Arduino::Arduino(std::string Device) : ParentSocket() {
	/* open serial port --> "/dev/ttyACM0" */
	this->Device = Device;
}

void ModPedalboard::Arduino::SetAttributes() {
	struct termios toptions;
	/* get current serial port settings */
	tcgetattr(fd, &toptions);
	
	/* set 9600 baud both ways */
	cfsetispeed(&toptions, B9600);
	cfsetospeed(&toptions, B9600);
	
	/* 8 bits, no parity, no stop bits */
	toptions.c_cflag &= ~PARENB;
	toptions.c_cflag &= ~CSTOPB;
	toptions.c_cflag &= ~CSIZE;
	toptions.c_cflag |= CS8;
	
	/* Canonical mode */
	toptions.c_lflag |= ICANON;
	
	/* commit the serial port settings */
	tcsetattr(fd, TCSANOW, &toptions);
}

void ModPedalboard::Arduino::OpenSocket() {
	fd = open(Device.c_str(), O_RDWR | O_NOCTTY);
	
	if(fd) {
		SetAttributes();
	}
}
