#include "Source/Bluetooth.h"
#include "Source/Socket.h"
#include "Source/PluginsManager.h"
#include "Source/Compression.h"

#include <thread>
#include <chrono>
#include <iostream>
#include <sstream>

#include <ctime>

using namespace std::chrono_literals;

void SendBluetoothData(ModPedalboard::Bluetooth &BTSocket, std::string &Buffer, int MaxSizeID = 4) {
	long int Index = 0, CurrentID = 0;
	const long int Length = Buffer.length();
	const int BTBufferSize = 990;
	const int MaxPkgSize = BTBufferSize - MaxSizeID - 1;
	
	
	//char Package[BTBufferSize+1];
	char Package[MaxSizeID + 1];
	char ID[MaxSizeID + 2];
	
	do {
		std::string tmpData = Buffer.substr(Index, MaxPkgSize);
		Index += tmpData.size();
		
		sprintf(ID, "%*ld", MaxSizeID, CurrentID++);
		for(int tmpIndexID = 0; tmpIndexID < MaxSizeID; tmpIndexID++) {
			Package[tmpIndexID] = ID[tmpIndexID];
		}
		Package[MaxSizeID] = ' ';
		Package[MaxSizeID + 1] = '\0';
		
		std::string tmpPackage(Package);
		tmpPackage.append(tmpData);
		
		BTSocket.SendData(tmpPackage);
		std::this_thread::sleep_for(50ms);
	} while(Index < Length);
}

int main(int argc, char **argv)
{
	std::cout << "HI... MODPEDALBOARD" << std::endl;
	
	system("sudo hciconfig hci0 up");
	system("sudo hciconfig hci0 noscan");
	system("sudo btmgmt -i hci0 connectable off");
	system("sudo btmgmt -i hci0 bondable off");
	system("sudo btmgmt -i hci0 pairable off");
	system("sudo btmgmt -i hci0 le off");
	system("sudo btmgmt -i hci0 fast-conn off");
	
	system("pkill jackd");
	system("pkill mod-host");
	//std::this_thread::sleep_for(3s);
	
	/*
	std::cout << "STARTING JACK\n" << std::endl;
	system("jackd -R -P90 -dalsa -dhw:0 -p64 -n3 -r48000 -s > /dev/null &");
	std::this_thread::sleep_for(2s);
	*/
	
	/*
	std::cout << "STARTING MOD-HOST\n" << std::endl;
	//system("/home/alarm/mod-host/mod-host --socket-port=1717 --feedback-port=1718");
	system("/home/alarm/mod-host/mod-host --socket-port=1717");
	std::this_thread::sleep_for(2s);
	*/
	
	/*
	ModPedalboard::Socket MySocket("localhost", 1717);
	ModPedalboard::Socket MySocket_Alt("localhost", 1718);
	MySocket.OpenSocket();
	MySocket_Alt.OpenSocket();
	*/
	
	ModPedalboard::Bluetooth BT_Socket("8ce255c0-200a-11e0-ac64-0800200c9a66", 1);
	BT_Socket.OpenSocket();
	
	/*
	if(!MySocket.IsOpen()) {
		perror("OPENING MAIN SOCKET");
		return -1;
	}
	
	if(!MySocket_Alt.IsOpen()) {
		perror("OPENING FEEDBACK SOCKET");
		return -1;
	}
	*/
	
	if(!BT_Socket.IsOpen()) {
		perror("OPENING BLUETOOTH SOCKET");
		return -1;
	}
	
	/*std::string DataRet;
	if(!MySocket.SendData("add http://guitarix.sourceforge.net/plugins/gx_amp#GUITARIX 1")) {
		perror("SENDING DATA");
		return -1;
	}
	DataRet = MySocket.ReadData();
	std::cout << "Mod-Host return -> " << DataRet << std::endl;
	
	std::this_thread::sleep_for(15s);
	
	if(!MySocket.SendData("remove 1")) {
		perror("SENDING DATA");
		return -1;
	}
	DataRet = MySocket.ReadData();
	std::cout << "Mod-Host return -> " << DataRet << std::endl;*/
	
	
	//std::this_thread::sleep_for(10s);
	
    auto start = std::chrono::high_resolution_clock::now();
	std::cout << "Loading Plugins" << std::endl;
	
	ModPedalboard::PluginsManager PluginsMngr;
	PluginsMngr.PreparePlugins_Data();
	PluginsMngr.LoadPlugins_Thread();
	PluginsMngr.UnifyPlugins_Data();
	
	auto finish = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsed = finish - start;
	
	std::stringstream strstream;
	PluginsMngr.PrintPlugins(strstream);
	std::string PluginsStream = strstream.str();
	
	/*
	auto t1 = std::chrono::high_resolution_clock::now();
	std::string CompressStream = compress_string(PluginsStream);
	auto t2 = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> t1t2 = t2 - t1;
	*/
	
	//std::cout << std::endl << PluginsStream << std::endl << std::endl;
	//std::cout << std::endl << "SIZE " << PluginsStream.length() << " COMPRESS " << CompressStream.length() << std::endl << std::endl;
	//std::cout << "TIME COMPRESS " << t1t2.count() << std::endl;
	
	std::cout << "Finish Loading Plugins " << elapsed.count() << std::endl;
	std::cout << std::endl;
	
	std::cout << "Waiting for Android Phone :)" << std::endl;
	
	BT_Socket.WaitConnection(30);
	
	int MaxSizeId = 4;
	char Buffer[50];
	//sprintf(Buffer, "PLUGINS_SIZE %u %u", CompressStream.length(), MaxSizeId);
	sprintf(Buffer, "PLUGINS_SIZE %u %u", PluginsStream.length(), MaxSizeId);
	BT_Socket.SendData(Buffer);
	
	//SendBluetoothData(BT_Socket, CompressStream);
	SendBluetoothData(BT_Socket, PluginsStream);
	std::this_thread::sleep_for(25s);
	
	return 0;
}

